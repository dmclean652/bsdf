# BSDF Java implementation

This is the implementation of the BSDF format for Java. It's in good
shape and well tested. This code supports everything required for
a minimal implementation. Also, the ndarry extension and user defined 
extensions are supported.

**NOTE:**  This source is written to Java 11 at least.

## Installation 

Download this source and use either the ant script or the maven build file to create the jar.

## Usage

Functionality is provided by creating a Data class, adding data to it, then using
the Encoder class to encode and decode.

```java
Data data = new Data();

data.add(1);
data.add(3.0);
data.add("Hello there");

HashMap<String, Object> map = new HashMap<>();
map.put("name2", new float[10]);
map.put("name4", new double[100000]);
data.add(map);

(new Encoder()).save(Path.of('output.bsdf'), data);
Data dataRead = (new Encoder()).load(Path.of('output.bsdf'));

if (dataRead.getType(0) == Data.Types.Long)
    long value = (Long)dataRead.get(0);
```

The `Extension` class can be extended in order to store and read your own data types.

## Limitations

The ndarray extension has been implemented and is used to translate to and from
native arrays of short, int, long, float. double. The notable exception in the ndarray
implementation is that uint16, uint32 and uint64 are not supported since those are
not supported in Java. It would be possible to move a uint16 to a in32 and a uint32 to an
int64 - that is yet to be coded. 

While you can pass a `boolean[]` to a `Data` object, the data will be saved as a list
of boolean and therefore returned as `List<Object>` that contains only `Boolean` values.Improving 
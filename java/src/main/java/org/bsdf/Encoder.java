package org.bsdf;

import org.bsdf.extensions.NDArrayExtension;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class Encoder
{
    private final List<Extension> extensions = new ArrayList<>();

    public Encoder()
    {
        extensions.add(new NDArrayExtension());
    }

    public Encoder addExtension(Extension e)
    {
        extensions.add(e);
        return this;
    }

    public Encoder addExtensions(List<Extension> e)
    {
        extensions.addAll(e);
        return this;
    }

    public ByteBuffer encode(Data data) throws IOException
    {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        save(bout, data);
        return ByteBuffer.wrap(bout.toByteArray());
    }

    public void save(File f, Data data) throws IOException
    {
        try (OutputStream out = new BufferedOutputStream(new FileOutputStream(f)))
        {
            save(out, data);
        }
    }

    public void save(Path p, Data data) throws IOException
    {
        try (OutputStream out = Files.newOutputStream(p, StandardOpenOption.WRITE))
        {
            data.write(out, extensions);
        }
    }

    public void save(OutputStream out, Data data) throws IOException
    {
        data.write(out, extensions);
    }

    public void save(WritableByteChannel out, Data data) throws IOException
    {
        save(Channels.newOutputStream(out), data);
    }

    public Data decode(ByteBuffer bb) throws IOException
    {
        return load(new ByteArrayInputStream(bb.array()));
    }

    public Data load(File f) throws IOException
    {
        try (InputStream in = new BufferedInputStream(new FileInputStream(f)))
        {
            return load(in);
        }
    }

    public Data load(Path p) throws IOException
    {
        return load(p.toFile());
    }

    public Data load(InputStream in) throws IOException
    {
        return new Data(in, extensions);
    }

    public Data load(ReadableByteChannel in) throws IOException
    {
        return new Data(Channels.newInputStream(in), extensions);
    }
}
